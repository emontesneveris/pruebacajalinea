package mx.isban.demomicroservicio.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("*")
public class ControladorCaja {

	
	@RequestMapping(value="/hola", method = RequestMethod.GET)
	public String getHello() {
		return "Hola Mundo";
	}
	
}
